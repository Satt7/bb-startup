Creating a new repository in bitbucket
Steps
1. From Bitbucket, click the + icon in the global sidebar and select Repository. Bitbucket displays the Create a new repository page.
2. Keep the rest of the options as is unless you want to change them: Access level—Leave the This is a private repository box checked.
3. Click Create repository.